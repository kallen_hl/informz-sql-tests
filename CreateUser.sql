DECLARE @brandId INT = 2587;
DECLARE @userName NVARCHAR(100) = 'kaTest';
DECLARE @userEmail NVARCHAR(100) = 'autounsubtest';
DECLARE @userPhone BIGINT = 5550001111;
DECLARE @userJobTitle NVARCHAR(100) = 'kaTest';
DECLARE @userPassword NVARCHAR(100) = 'kaTest';
DECLARE @firstName  NVARCHAR(100) = 'kaTest';
DECLARE @lastName  NVARCHAR(100) = 'kaTest';
DECLARE @subscriberId INT;
-- Delete Pre-existing Records
-- DELETE from subscriber where subscriber_email = @userEmail and brand_id = @brandId;
-- DELETE from user_logins where subscriber_username = @userName and brand_id = @brandId;
INSERT INTO subscriber
    (brand_id, subscriber_email, subscriber_job_title,
    subscriber_phone_number)
VALUES
    ( @brandId, @userEmail, @userJobTitle, @userPhone);
SELECT @subscriberId = SCOPE_IDENTITY();
DECLARE @passwordHash NVARCHAR(500) = dbo.hash_user_password(NULL, @userPassword);
INSERT INTO user_logins
    (subscriber_id, subscriber_firstname, subscriber_lastname,
    subscriber_password_hash, subscriber_username, brand_id, created_by,
    modified_by )
VALUES(@subscriberId , @firstName, @lastName, @passwordHash, @userName,  @brandId, 0, 0);
INSERT INTO permissions_to_user SELECT permission_code, @subscriberId as varchar
                    FROM dbo.fn_getNormalizedPermissions('!+>#~$=^123bcefgikmnopqrstuwxyz');
INSERT INTO user_prefs(subscriber_id, timezone_id) VALUES(@subscriberId, 26);
-- See our new subcriber / user login
SELECT * from subscriber where subscriber_email = @userEmail and brand_id =  @brandId;
SELECT * from user_logins where subscriber_username = @userName and brand_id =  @brandId;
SELECT * FROM TARGET_GROUP WHERE TARGET_GROUP_NAME like 'Auto Resend:%' order by 1 desc;
