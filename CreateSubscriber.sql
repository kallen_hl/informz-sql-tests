DECLARE @brandId INT = <enter brand id>;
DECLARE @subscriberEmail NVARCHAR(100) = '<enter subscriber email';
DECLARE @userPhone BIGINT = 5550001111;
DECLARE @userJobTitle NVARCHAR(100) = '<enter job title>';
DECLARE @subscriberId INT;
-- Delete Pre-existing Records
-- DELETE from subscriber where subscriber_email = @subscriberEmail and brand_id = @brandId;
-- DELETE from user_logins where subscriber_username = @userName and brand_id = @brandId;
INSERT INTO subscriber
    (brand_id, subscriber_email, subscriber_job_title,
    subscriber_phone_number)
VALUES
    ( @brandId, @subscriberEmail, @userJobTitle, @userPhone);
SELECT * FROM subscriber where subscriber_email = @subscriberEmail;
